#! /usr/bin/python

import os
import time
import urllib.request
import pygame.midi

from http.server import HTTPServer
from http.server import SimpleHTTPRequestHandler
from string import Template
from configparser import ConfigParser, NoOptionError
from pythonosc import osc_message_builder
from pythonosc import udp_client

# https://pypi.python.org/pypi/python-osc/1.3

__author__ = 'victor'
__version__ = "0.1"

_wwwdir = ""

class Configuration():

    def __init__(self):
        global _wwwdir

        # [PageDescriptor]
        self.pages = {}

        cfg = ConfigParser()
        cfg.read("remict.ini")

        midi_name = cfg.get("DEFAULT", "midiout", fallback="Not defined Midi!")
        pygame.midi.init()
        self.midi_receiver = None
        for i in range(pygame.midi.get_count()):
            info = pygame.midi.get_device_info(i)
            if not info[3]:
                continue
            name = info[1].decode()
            if name.find(midi_name) >= 0:
                self.midi_receiver = pygame.midi.Output(i)
                print("using: ", name)

        self.port_number = cfg.getint("DEFAULT", "port", fallback=80)
        self.host_name = cfg.get("DEFAULT", "host", fallback="0.0.0.0")
        _wwwdir = cfg.get("DEFAULT", "wwwdir", fallback="templates/")

        osc = cfg.get("DEFAULT", "osc.out", fallback="").split(":")
        if osc and (len(osc) == 2):
            self.osc_receiver = udp_client.UDPClient(osc[0], int(osc[1]))
        cc = cfg.get("DEFAULT", "osc.cc", fallback="/remote")

        # '<a href="/$href" target="_top" class="ui-btn ui-btn-inline ui-btn-b">$label</a>'
        button_format = cfg.get("DEFAULT", "tmpl.index.button")

        text = ""

        for p in cfg.get("DEFAULT", "pages").strip().split(","):
            pd = PageDescriptor(button_format, self.midi_receiver, self.osc_receiver, cc)
            pd.title = p
            try:
                sliders = cfg.get(p, "sliders").strip().split(";")
                for c in sliders:
                    if c.strip() == "":
                        continue
                    ctl = SliderCtrlDescriptor(c.strip())
                    pd.controls.append(ctl)
            except NoOptionError:
                print("No sliders in ", p)

            try:
                switches = cfg.get(p, "switches").strip().split(";")
                for c in switches:
                    if c.strip() == "":
                        continue
                    ctl = SwitchCtrlDescriptor(c.strip())
                    pd.controls.append(ctl)
            except NoOptionError:
                print("No switches in ", p)

            if len(pd.controls) == 0:
                print("WARNING: No controls on page " + p)
            else:
                text += pd.make_html_list_element(p)
                self.pages[p] = pd

        te = Template(read_template(_wwwdir + cfg.get("DEFAULT", "tmpl.index")))
        self.index_page = bytearray(te.substitute(pages=text), "UTF-8")
        self.page_template = read_template(_wwwdir + cfg.get("DEFAULT", "tmpl.page"))


def read_template(file_name):
    with open(file_name) as f:
        s = f.readlines()
        return "".join(s)


def update_page_values(u, pg):
    u = urllib.request.unquote(u)
    pq = u.split("?", 2)
    if len(pq) < 2:
        return

    qq = pq[1].split("&")
    for q in qq:
        kv = q.partition("=")
        if len(kv) == 3:
            pg.update(kv[0], kv[2])


class CtrlDescriptor():
    def __init__(self, c):
        cc = c.split(":")
        if len(cc) != 4:
            raise Exception("FATAL: Invalid control '" + c + "'")
        self.title = cc[0].strip()
        self.value = cc[1].strip()
        self.channel = int(cc[2].strip())
        self.cmd = int(cc[3].strip())


class SliderCtrlDescriptor(CtrlDescriptor):
    def __init__(self, c):
        CtrlDescriptor.__init__(self, c)
        self.value = float(self.value)

    # // Entries like {"v":0.33, "t":"one", "s":"false"},
    def make_java_script_value(self):
        te = Template('{"v":"$v","t":"$t","s":"false"}')
        return te.substitute(v=self.value, t=self.title)


class SwitchCtrlDescriptor(CtrlDescriptor):
    def __init__(self, c):
        CtrlDescriptor.__init__(self, c)

    # // Entries like {v:true, t:"one", s:"true"},
    def make_java_script_value(self):
        te = Template('{"v":$v,"t":"$t","s":"true"}')
        return te.substitute(v=self.value, t=self.title)


class PageDescriptor():

    def __init__(self, bf, mr, osc, cc):
        self.button_format = bf
        self.title = None
        self.controls = []
        self.midi_receiver = mr
        self.osc_receiver = osc
        self.osc_cc = cc

    #	// Entries like <li><a href="page1">Volume</a></li>,
    def make_html_list_element(self, key):
        t = Template(self.button_format)
        return t.substitute(href=key, label=self.title)

    def make_html_slider_values(self):
        s = []
        for ctl in self.controls:
            s.append(ctl.make_java_script_value())
        return ",".join(s)

    def update(self, t, v):
        for ctl in self.controls:
            if ctl.title == t:
                val = 0
                if isinstance(ctl, SliderCtrlDescriptor):
                    fval = float(v)
                    ctl.value = fval
                    if fval > 1:
                        fval = 1
                    val = int(fval * 127)
                else:
                    if isinstance(ctl, SwitchCtrlDescriptor):
                        if v == "true":
                            ctl.value = "true"
                            val = 127
                        else:
                            ctl.value = "false"
                            val = 0

                if self.midi_receiver is not None:
                    self.midi_receiver.write_short(0xb0 + ctl.channel, ctl.cmd, val)
                if self.osc_receiver:
                    msg = osc_message_builder.OscMessageBuilder(address = self.osc_cc)
                    msg.add_arg(ctl.channel)
                    msg.add_arg(ctl.cmd)
                    msg.add_arg(val)
                    msg = msg.build()
                    self.osc_receiver.send(msg)
                break


class ReMiHandler(SimpleHTTPRequestHandler):

    def log_request(self, code='-', size='-'):
        """Overwrites the base class method to disable console output for each request

        @param code: ignored
        @param size: ignored
        """
        pass

    def end_headers(self):
        """Inserts Expires method

        """
        if self.expiration:
            self.send_header("Expires", self.date_time_string(time.time() + self.expiration))
        SimpleHTTPRequestHandler.end_headers(self)

    def do_GET(self):
        self.expiration = 0

        # is it update for one of the pages?
        if self.path.startswith("/update"):
            pg = urllib.request.unquote(self.path[len("/update") + 1:].split("?", 1)[0])
            if pg in config.pages:
                update_page_values(self.path, config.pages[pg])
                pg = "[" + config.pages[pg].make_html_slider_values() + "]"
                self.send_response(200)
                self.send_header("Cache-Control", "no-cache, no-store")
                self.send_header("Content-type", "text/plain")
                self.send_header("Content-Length", str(len(pg)))
                # already expired
                self.expiration = -1000
                self.end_headers()
                self.wfile.write(bytearray(pg, "UTF-8"))
        elif self.path.startswith('/jQuery'):
            self.path = _wwwdir + self.path
            self.expiration = 60*60*24*31*12
            SimpleHTTPRequestHandler.do_GET(self)
        elif self.path == '/':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.send_header("Content-Length", str(len(config.index_page)))
            self.end_headers()
            self.wfile.write(config.index_page)
        else:
            pg = urllib.request.unquote(self.path[1:].split("?", 1)[0])
            self.send_response(200)
            if pg in config.pages:
                update_page_values(self.path, config.pages[pg])
                pg = config.page_template.replace("{{{CONTROLS}}}", config.pages[pg].make_html_slider_values())
                self.send_header("Cache-Control", "no-cache, no-store")

            self.send_header("Content-type", "text/html")
            self.send_header("Content-Length", str(len(pg)))
            self.end_headers()
            self.wfile.write(bytearray(pg, "UTF-8"))

def main():
    global config
    config = Configuration()
    print(time.asctime(), "Server Starts - %s:%s at %s" % (config.host_name, config.port_number, os.getcwd()))

    httpd = HTTPServer((config.host_name, config.port_number), ReMiHandler)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()

    if config.midi_receiver is not None:
        del config.midi_receiver
    pygame.midi.quit()
    print(time.asctime(), "Server Stops - %s:%s" % (config.host_name, config.port_number))

if __name__ == '__main__':

    main()
