#import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [], include_files = ["remict.ini", "templates"])

base = 'Console'
#if sys.platform == "win32":
#    base = "Win32GUI"

executables = [
    Executable('__main__.py', base=base)
]

setup(name='ReMiCt',
      version = '1.0',
      description = 'Remote MIDI Controller',
      options = dict(build_exe = buildOptions),
      executables = executables)
